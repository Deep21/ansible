#!/bin/bash

# Update hosts file
echo "[TASK 1] Update /etc/hosts file"

cat >>/etc/hosts<<EOF
192.168.50.100 master.example.com master
192.168.50.101 worker1.example.com worker1
192.168.50.102 worker2.example.com worker2
EOF

# Install docker from Docker-ce repository
echo "[TASK 2] Install docker container engine"
 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - 
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update  > /dev/null 2>&1
sudo apt-get install docker-ce -y  > /dev/null 2>&1

# Disable swap
echo "[TASK 3] Disable and turn off SWAP"
sed -i '/swap/d' /etc/fstab
swapoff -a


# Add yum repo file for Kubernetes
echo "[TASK 4] Add yum repo file for kubernetes"
# Kubernetes
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat << EOF > /etc/apt/sources.list.d/kubernetes.list  
# Yes, xenial on trusty
deb http://apt.kubernetes.io/ kubernetes-xenial main  
EOF
  

# Install Kubernetes
echo "[TASK 5] Install Kubernetes (kubeadm, kubelet and kubectl)"
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
sudo apt-get update
sudo apt-get install kubeadm -y